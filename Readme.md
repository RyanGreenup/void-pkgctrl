# void-pkgctrl


`void-pkgctrl` is a package management tool for Void Linux, inspired by the functionality of `nix-env -if default.nix`. Nix is great for most things, but it isn't always the best option, e.g. `docker`,  `flatpak`, `openssh` or `nix` itself. `void-pkgctrl` offers an additional approach that can be used with a wider range of packages.

`void-pkgctrl` was born out of a desire to make the setup of a new machine more reproducible. The tool allows you to install and remove packages based on a text file containing a list of packages to install or remove, if you want to know what packages you currently have installed as a starting point you can run:


```bash
# Void
xbps-query -lm | rev | cut -d '-' -f 2- | rev

# Arch (Not Supported <yet?>)
pacman -Qe
```

The tool was originally developed for use with Void Linux, but I have also attempted to make it work with Arch Linux. Unfortunately, I was unable to find a simple way to determine the dependencies of specific packages in Arch Linux, which makes it difficult to ensure that all dependencies are installed automatically. Therefore, you can't yet use this with Arch, I recommend creating meta-packages for a similar approach.

If you use Arch or something else, PRs always welcome.

However, if you use Void Linux, you can use `void-pkgctrl` to make your package management more reproducible and consistent across machines.

Give it a try and see how it works for you!


## Usage

```bash
# Create a list of packages
xbps-query -lm | rev | cut -d '-' -f 2- | rev > ~/default.txt

# Install all those packages and remove anything else
void-pkgctrl ~/default.nix
```


### Recommended Approach

  1. Use this
  2. Use Nix

#### void-pkgctrl
Install it:

```bash
cd $(mktemp -d)
git clone --depth 1 'https://gitlab.com/RyanGreenup/void-pkgctrl'
cd void-pkgctrl
cargo install --path .
```

Add some default packages to `~/default.pkg`:

```
docker
docker-compose
nix
flatpak
opendoas
openssh
cronie

# As required:
# void-repo-nonfree
```

Then install them with:


```bash
void-pkgctrl ~/default.pkg
```

#### nix

Now that the basics are set up with void, use nix to get all the other stuff:

```bash
# Set up nix
xbps-install nix
doas ln -s /etc/sv/nix-daemon/ /var/service/
nix-channel --add https://nixos.org/channels/nixpkgs-unstable
nix-channel --update
```

Create a default file:

```nix
# Import the nixpkgs library
let
  pkgs = import <nixpkgs> {};
in
  # Build a Nix environment with specified packages
  pkgs.buildEnv {
    # Set the name of the environment
    name = "my-packages";
    # Set the paths to the desired packages
    paths = with pkgs; [
    # Editors
    neovim
    helix
    lapce

    # Browsers
    librewolf
    brave

    ];
  }

# Instructions for using the default.nix file
## nix-channel --add https://nixos.org/channels/nixpkgs-unstable
## nix-channel --update
## nix-env -if default.nix
```

Run Nix:

```bash
nix-env -if default.nix
```


#### Adding and removing packages

```bash
cd ~/.local/share
nvim -O default.nix default.pkgs
nix-env -if default.nix; void-pkgctrl default.pkgs
```
