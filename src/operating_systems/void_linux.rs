use duct::cmd;

use crate::operating_systems::*;

#[derive(Debug)]
pub struct VoidLinux {}

impl Packages for VoidLinux {
    /// Checks if a package is a dependency of any other installed package
    ///
    /// This function takes a package name as input and returns a boolean indicating whether
    /// the package is a dependency of any other installed package on the system. It uses the
    /// `xbps-query` command to check for packages that depend on the given package.
    ///
    /// # Arguments
    ///
    /// * `package` - A string containing the name of the package to check for dependencies.
    ///
    /// # Returns
    ///
    /// A boolean value indicating whether the package is a dependency of any other installed package.
    ///
    /// # Errors
    ///
    /// This function will panic if it is unable to run the `xbps-query` command to obtain the
    /// list of package dependants. If there is an error in the `xbps-query` command, it will be
    /// passed through to the caller.
    ///
    /// # Examples
    ///
    /// ```
    /// let package_name = "libssl1.1";
    /// let is_dependent = is_a_dependent(package_name.to_string());
    /// println!("{} is a dependent: {}", package_name, is_dependent);
    /// ```
    fn is_a_dependent(&self, package: String) -> bool {
        let dependants: Vec<String> = cmd!("xbps-query", "-X", package)
            .read()
            .expect("Unable to get Available dependants from xbps-query -X {package}")
            .lines()
            .map(String::from)
            .collect();

        dependants.len() > 0
    }
    fn p(&self) {
        println!("void");
    }
    /*
       To get currently available packages for void:
           xbps-query -Rs '' | grep "^i" | awk '{print $2}' | rev | cut -d - -f 2- | rev
    */
    fn available(&self) -> Vec<String> {
        cmd!("xbps-query", "-Rs", "")
            .pipe(cmd!("awk", r#"{ print $2 }"#))
            .pipe(cmd!("rev"))
            .pipe(cmd!("cut", "-d", "-", "-f", "2-"))
            .pipe(cmd!("rev"))
            .read()
            .expect("Unable to get Available Packages from xbps-query -Rs ''")
            .lines()
            .map(String::from)
            .collect()
    }
    /*
       To get currently installed packages for void:
           xbps-query -l | grep "^i" | awk '{print $2}' | rev | cut -d - -f 2- | rev
       To get explicit packages (not dependencies):
           xbps-query -lm | rev | cut -d '-' -f 2- | rev
    */
    fn installed(&self) -> Vec<String> {
        cmd!("xbps-query", "-l")
            .pipe(cmd!("awk", r#"/^i/ { print $2 }"#))
            .pipe(cmd!("rev"))
            .pipe(cmd!("cut", "-d", "-", "-f", "2-"))
            .pipe(cmd!("rev"))
            .read()
            .expect("Unable to get list of installed packages")
            .lines()
            .map(String::from)
            .collect()
    }

    fn do_install(&self, path: &std::path::Path) -> Result<(), Box<dyn std::error::Error>> {
        // Get the Packages to install
        let packages = self.needed(path);
        // Make a command containing each package to install
        let c = make_cmd(
            "doas",
            vec!["xbps-install".to_string(), "-S".to_string()],
            packages,
        )?;
        // Run the command
        c.run()?;

        Ok(())
    }

    fn remove(&self, path: &std::path::Path) -> Result<(), Box<dyn std::error::Error>> {
        // Create a vector of arguments
        let args = vec![
            // TODO annotate
            "xbps-remove".to_string(),
            "-OoRyv".to_string(),
        ];

        let packages = self.un_needed(path);
        println!("\n\nThe following packages are marked for removal:");
        println!("{:#?}\n\n", packages);
        let c = make_cmd("doas", args, packages)?;
        c.run()?;

        Ok(())
    }
}
