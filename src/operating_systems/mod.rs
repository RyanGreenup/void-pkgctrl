use duct::cmd;
use rayon::prelude::*;
use std::collections::HashSet;

pub mod void_linux;

// An enum to store the operating system whether void linux arch linux debian etc
#[derive(Debug)]
pub enum OperatingSystem {
    Void,
    Arch,
    // Debian,
    // Ubuntu,
    // Fedora,
    // OpenSUSE,
    // Gentoo,
    // Alpine,
    // NixOS,
    Other,
}

// method of the enum to get the operating system
impl OperatingSystem {
    pub fn get_os() -> OperatingSystem {
        let os = std::env::consts::OS;
        match os {
            "linux" => {
                let distro = get_distro();
                match distro.as_str() {
                    "void" => OperatingSystem::Void,
                    "arch" => OperatingSystem::Arch,
                    // "debian" => OperatingSystem::Debian,
                    // "ubuntu" => OperatingSystem::Ubuntu,
                    // "fedora" => OperatingSystem::Fedora,
                    // "opensuse" => OperatingSystem::OpenSUSE,
                    // "gentoo" => OperatingSystem::Gentoo,
                    // "alpine" => OperatingSystem::Alpine,
                    // "nixos" => OperatingSystem::NixOS,
                    _ => OperatingSystem::Other,
                }
            }
            _ => OperatingSystem::Other,
        }
    }
}

// method to get the distribution of the operating system
pub fn get_distro() -> String {
    // read the /etc/os-release file in as a string
    std::fs::read_to_string("/etc/os-release")
        .expect("Unable to read /etc/os-release")
        // match the line that has ID = ...
        .lines()
        .find(|line| line.starts_with("ID="))
        .expect("Unable to find ID= in /etc/os-release")
        // remove the ID= and the quotes and trim the string
        .replace("ID=", "")
        .replace("\"", "")
        .trim()
        .to_string()
}


/*
Currently the next step here is to fix get_deps collect_deps and needed_deps

they do not build a working dependency tree and so the packages.txt must be overly explicit
ideally it should only list manual packages not ALL packages including dependencies that would otherwise be irellevant

the problem with listing dependencies in the packages.txt is that they will then all be switched over to explicit/manual rather than dependent packages
meaning every package on the system will be considered an explicit package that the unser intendend
to install. e.g. libcap is not a package a user would want but rather a dependency of bubblewrap or
coreutils.


This works for Void because xbps-query -X will list which packages are dependant of a removal-candidate package
*/

pub trait Packages {
    fn needed(&self, path: &std::path::Path) -> Vec<String> {
        let packages = read_package_list(&path).expect("unable to read package list");
        let available = self.available();
        intersection(packages, available)
    }
    fn un_needed(&self, path: &std::path::Path) -> Vec<String> {
        let installed = self.installed();
        let needed = self.needed(path);

        println!("Determining which additional Packages are not needed as dependencies of those in the package list...");

        complement(installed, needed)
            // make into a parallelized iterator
            .into_par_iter()
            .filter(|package| !is_a_dependent(package.to_string(), OperatingSystem::get_os()))
            .collect()
    }
    fn is_a_dependent(&self, _: String) -> bool;
    fn available(&self) -> Vec<String>;
    fn installed(&self) -> Vec<String>;
    fn do_install(&self, path: &std::path::Path) -> Result<(), Box<dyn std::error::Error>>;
    fn remove(&self, path: &std::path::Path) -> Result<(), Box<dyn std::error::Error>>;
    fn p(&self);
}

// NOTE this use of an enum is not consistent with the approach of using methods
// but it makes handling the multi threaded part easier because self can't be passed to a thread
// and the enum is a way to pass the data to the thread
// this is a bit of a hack and should be reworked
// but it works for now
pub fn is_a_dependent(package: String, os: OperatingSystem) -> bool {
    match os {
        OperatingSystem::Void => {
            let dependants: Vec<String> = cmd!("xbps-query", "-X", package)
                .read()
                .expect("Unable to get Available dependants from xbps-query -X {package}")
                .lines()
                .map(String::from)
                .collect();

            dependants.len() > 0
        }
        // NOTE this means any other system doesn't have a way to check if a package is a dependency
        _ => false,
    }
}





pub fn make_cmd(
    command: &str,
    mut args: Vec<String>,
    packages: Vec<String>,
) -> Result<duct::Expression, Box<dyn std::error::Error>> {
    // Add the packages to the arguments vector
    args.extend(packages);

    // Call the command on all packages
    let c = cmd(command, &args);

    Ok(c)
}

pub fn complement(a: Vec<String>, b: Vec<String>) -> Vec<String> {
    let set_a: HashSet<String> = a.into_iter().collect();
    let set_b: HashSet<String> = b.into_iter().collect();
    let complement: HashSet<_> = set_a.difference(&set_b).collect();
    complement.into_iter().cloned().collect()
}

pub fn intersection(a: Vec<String>, b: Vec<String>) -> Vec<String> {
    let set_a: HashSet<String> = a.into_iter().collect();
    let set_b: HashSet<String> = b.into_iter().collect();
    let intersection: HashSet<_> = set_a.intersection(&set_b).collect();
    intersection.into_iter().cloned().collect()
}

pub fn read_package_list(path: &std::path::Path) -> Result<Vec<String>, Box<dyn std::error::Error>> {
    let content = std::fs::read_to_string(path)?;
    let package_list = content
        .lines()
        .filter(|s| !s.starts_with('#'))
        .filter(|s| !s.starts_with(r#"//"#))
        .map(String::from)
        .collect();

    Ok(package_list)
}
