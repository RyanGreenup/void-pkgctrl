use crate::operating_systems::void_linux::*;
use crate::operating_systems::*;

pub mod operating_systems;

fn main() {
    // first argument is package file
    if std::env::args().len() < 2 {
        println!("No package file provided");
        std::process::exit(1);
    } else {
        let p = std::env::args().nth(1).expect("No package file provided");
        run(&p).expect("Unable to run");
    }
}
/// Detects the operating system and installs/removes packages according to the OS-specific implementation.
///
/// This function uses the `get_os` method to detect the current operating system and selects an
/// appropriate implementation of the `Packages` trait based on the detected OS. The function then
/// calls the `p` method on the selected implementation to print the detected operating system.
/// After that, it installs the packages listed in the file located at `/home/ryan/packages.txt`
/// using the `do_install` method, and then removes the remaining packages listed in the same file
/// using the `remove` method.
///
/// # Arguments
///
/// This function takes no arguments.
///
/// # Returns
///
/// This function returns a `Result` indicating whether the operation was successful. If an error occurs, an error message is returned as a boxed error.
///
/// # Errors
///
/// This function returns an error if any of the underlying package installation/removal operations fail. The error message includes a description of the error and a suggested resolution.
///
/// # Examples
///
/// ```
/// use crate::run;
///
/// fn main() {
///     run().unwrap_or_else(|err| println!("Error: {}", err));
/// }
/// ```
///
/// # See also
///
/// * [`Packages`](trait.Packages.html): the trait implemented by OS-specific package managers.
fn run(package_file: &str) -> Result<(), Box<dyn std::error::Error>> {
    let os = OperatingSystem::get_os();
    println!("Operating System: {:?}", os);
    let distro: Box<dyn Packages> = match os {
        OperatingSystem::Void => Box::new(VoidLinux {}),
        // TODO other Operating Systems
        // OperatingSystem::Arch => Box::new(Arch {}),
        _ => {
            panic!("Could not detect OS");
        }
    };

    // Print the detected operating system
    distro.p();

    // Install all those packages
    distro
        .do_install(&std::path::Path::new(package_file))
        .expect("Unable to install packages");

    // Remove remaining packages
    distro
        .remove(&std::path::Path::new(package_file))
        .expect("Unable to install packages");

    Ok(())
}
